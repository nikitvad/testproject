package com.example.myapplication

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.data.EntityObject
import kotlinx.android.synthetic.main.layout_entity_item.view.*

class EntityAdapter(val items : List<EntityObject>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_entity_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.tag?.text = items.get(position).status?.tag?.toString()
        holder?.name?.text = items.get(position).name
        holder?.title?.text = items.get(position).title.toString()
    }

    override fun getItemCount(): Int {
        return items.size
    }

}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val tag = view.tagView
    val name = view.name
    val title = view.title
}