package com.example.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.list.ListViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val listViewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)

        listViewModel.loadEntity()
        listViewModel.entity.observe(this, Observer {
            recyclerList.layoutManager = LinearLayoutManager(this)
            recyclerList.adapter = EntityAdapter(it, this)
        })

    }
}
