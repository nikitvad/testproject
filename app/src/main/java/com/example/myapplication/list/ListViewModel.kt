package com.example.myapplication.list

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.api.ApiClient
import com.example.myapplication.data.EntityObject
import com.example.myapplication.data.Status
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class ListViewModel : ViewModel() {

    val apiService = ApiClient().apiService

    public var entity: MutableLiveData<List<EntityObject>> = MutableLiveData()
    private var statuses: HashMap<Int, Status> = HashMap()

    val regex = Regex("\\D+")

    public fun loadEntity() {

        val observableEntity = apiService.getEntity()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap {
                Observable.just(it.objects?.objectX)
            }

        val observableStatuses = apiService.getStatuses()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap {
                val hashMap = HashMap<Int, Status>()

                for (objectX in it) {
                    if (objectX.status?.objectId != null) {
                        hashMap[objectX.status?.objectId!!] = objectX.status!!
                    }
                }
                Observable.just(hashMap)
            }
            .subscribe {
                statuses = it
                observableEntity
                    .flatMap {

                        val sortedList = it.map { it ->
                            it?.status = statuses[it?.objectId]
                            return@map it
                        }
                            .sortedWith(compareBy({ it?.status?.tag },{it?.name?.replace(regex, "")?.toInt()}, { it?.name }, { it?.title })
                        ).reversed()
                        Observable.just(sortedList)
                    }
                    .subscribe {
                        entity.value = it as List<EntityObject>?
                    }
            }

    }


    override fun onCleared() {
        super.onCleared()
        loadEntity()
    }
}