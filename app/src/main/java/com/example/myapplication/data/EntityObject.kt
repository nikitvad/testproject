package com.example.myapplication.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class EntityObject(
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("object_id")
    var objectId: Int? = null,
    @SerializedName("title")
    var title: String? = null,

    var status: Status? = null

)