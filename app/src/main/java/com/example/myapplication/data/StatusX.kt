package com.example.myapplication.data

import com.google.gson.annotations.SerializedName

data class StatusX(
    @SerializedName("status")
    var status: Status? = null
)