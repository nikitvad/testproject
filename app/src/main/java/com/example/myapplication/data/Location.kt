package com.example.myapplication.data

import com.google.gson.annotations.SerializedName

data class Location(
    @SerializedName("gps_lat")
    var gpsLat: Double? = null,
    @SerializedName("gps_lng")
    var gpsLng: Double? = null
)