package com.example.myapplication.data

import com.google.gson.annotations.SerializedName

data class Status(
    @SerializedName("object_id")
    var objectId: Int? = null,
    @SerializedName("tag")
    var tag: Int? = null
)