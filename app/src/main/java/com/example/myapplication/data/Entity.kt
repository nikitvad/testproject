package com.example.myapplication.data

import com.google.gson.annotations.SerializedName

data class Entity(
    @SerializedName("location")
    var location: Location? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("objects")
    var objects: EntityObjects? = null
)