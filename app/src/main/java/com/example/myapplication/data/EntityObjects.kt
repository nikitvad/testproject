package com.example.myapplication.data

import com.google.gson.annotations.SerializedName

data class EntityObjects(
    @SerializedName("object")
    var objectX: MutableList<EntityObject?>? = null
)