package com.example.myapplication.api

import com.example.myapplication.data.Entity
import com.example.myapplication.data.Status
import com.example.myapplication.data.StatusX
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface ApiService{

    @GET("/s/c9o1x8i45q5872k/statuses.json")
    public fun getStatuses(): Observable<List<StatusX>>

    @GET("/s/ufwuccum01rchdl/entity.json")
    public fun getEntity(): Observable<Entity>

}
